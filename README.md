# README #

Manual: Remotely-sensed habitat indicators in R

Christine Wallis


### What is this repository for? ###

A spatially explicit indicator for species diversity derived from remote sensing in the mountain rainforest of southern Ecuador

August 2017

Author: Christine Wallis

For more information contact: christine.wallis@staff.uni-marburg.de

### How do I get set up? ###

Summary: Based on the examplary study, the aim of this manual is to model species richness of geometrid moths in southern Ecuador using remotely-sensed habitat indicators. Several excercises introduce how to 

* (a) download remote sensing data and species richness data
* (b) pre-process open access satellite images (Landsat-8, Aster Global DEM),
* (c) calculate habitat indicators and
* (d) derive statistical models.

### Who do I talk to? ###

* Contact: Christine Wallis, christine.wallis@staff.Uni-Marburg.DE
* Repo admin: Maik Dobbermann, dobberma@staff.uni-marburg.de
